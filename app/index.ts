import './style.css';

let variable = 2;

var element = document.createElement('div');

element.innerHTML = 'Welcome';
document.body.appendChild(element);

import {MustHaveCoffee} from "./secondary"

class SweetSweetClass {
    constructor() { 
        console.log("Even sweeter")
    }
}

let basil = new SweetSweetClass()
let coffee = new MustHaveCoffee()